FROM alpine:3.11.2
RUN apk add --no-cache openjdk11
COPY build/libs/ms7-k8s-spring-1.0.fad1407.jar /app/
WORKDIR /app/
ENTRYPOINT ["java"]
CMD ["-jar", "/app/ms7-k8s-spring-1.0.fad1407.jar"]
