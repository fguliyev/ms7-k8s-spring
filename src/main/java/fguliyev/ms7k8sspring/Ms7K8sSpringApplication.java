package fguliyev.ms7k8sspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ms7K8sSpringApplication {

    public static void main(String[] args) {
        SpringApplication.run(Ms7K8sSpringApplication.class, args);
    }

}
