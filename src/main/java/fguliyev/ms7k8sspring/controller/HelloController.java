package fguliyev.ms7k8sspring.controller;

import fguliyev.ms7k8sspring.dto.HelloDto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hello")
public class HelloController {

    @GetMapping
    public HelloDto sayHello() {
        return new HelloDto("Hello from Spring");
    }
}
